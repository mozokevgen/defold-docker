#!/bin/sh

set -e

# split version into major
version=${DEFOLD_API_VERSION:1}
version=${version//./ }
major=$(echo $version | awk '{print $1}')
minor=$(echo $version | awk '{print $2}')
patch=$(echo $version | awk '{print $3}')

# tag it with full version
echo "pushing ${IMAGE_NAME}:${major}.${minor}.${patch}"
docker tag $IMAGE_NAME ${IMAGE_NAME}:${major}.${minor}.${patch}
docker push ${IMAGE_NAME}:${major}.${minor}.${patch}

# tag it with major and minor number
echo "pushing ${IMAGE_NAME}:${major}.${minor}"
docker tag $IMAGE_NAME ${IMAGE_NAME}:${major}.${minor}
docker push ${IMAGE_NAME}:${major}.${minor}

# tag it with major number
echo "pushing ${IMAGE_NAME}:${major}"
docker tag $IMAGE_NAME ${IMAGE_NAME}:${major}
docker push ${IMAGE_NAME}:${major}

# tag it with latest
echo "pushing ${IMAGE_NAME}:latest"
docker tag $IMAGE_NAME ${IMAGE_NAME}:latest
docker push ${IMAGE_NAME}:latest
